﻿namespace Atlas.Modelos
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Classe principal que recebe toda a informação acerca de cada país
    /// </summary>
    public class Pais
    {
        public string Name { get; set; }
        public string Capital { get; set; }
        public string Region { get; set; }
        public string Subregion { get; set; }
        public int Population { get; set; }
        public string Demonym { get; set; }
        public string Area { get; set; }
        public string NativeName { get; set; }
        public string NumericCode { get; set; }
        public string Flag { get; set; }
        public string Cioc { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencySymbol { get; set; }
        public string Domain { get; set; }
        public List<Currency> Currencies { get; set; }
        public List<string> TopLevelDomain { get; set; }


        /// <summary>
        /// Testa se o valor é null e torna-o numa string vazia
        /// C# entra em conflito com valores 'null' no SQLite
        /// </summary>
        public void CurrencySymbolNotNull()
        {
            if (CurrencySymbol == null)
            {
                CurrencySymbol = "";
            }
        }

        /// <summary>
        /// Testa se o valor é null e torna-o numa string vazia
        /// C# entra em conflito com valores 'null' no SQLite
        /// </summary>
        public void AreaNotNull()
        {
            if (Area == null)
            {
                Area = "";
            }
        }

        /// <summary>
        /// Testa se o valor é null e torna-o numa string vazia
        /// C# entra em conflito com valores 'null' no SQLite
        /// </summary>
        public void CiocNotNull()
        {
            if (Cioc == null)
            {
                Cioc = "";
            }
        }

        /// <summary>
        /// Testa se o valor é null e torna-o numa string vazia
        /// C# entra em conflito com valores 'null' no SQLite
        /// </summary>
        public void NumericCodeNotNull()
        {
            if (NumericCode == null)
            {
                NumericCode = "";
            }

        }

        /// <summary>
        /// Acerta valores de certos países para um melhor display
        /// </summary>
        public void SetCurrency()
        {
            if (Currencies.Count > 0)
            {
                if (Name == "Virgin Islands (British)")
                {
                    CurrencyCode = Currencies[1].Code;
                    CurrencyName = Currencies[1].Name;
                    CurrencySymbol = Currencies[1].Symbol;
                }
                else if (Name == "Palau")
                {
                    CurrencyCode = Currencies[1].Code;
                    CurrencyName = Currencies[1].Name;
                    CurrencySymbol = Currencies[1].Symbol;
                }
                else if (Name == "Micronesia (Federated States of)")
                {
                    CurrencyCode = Currencies[1].Code;
                    CurrencyName = Currencies[1].Name;
                    CurrencySymbol = Currencies[1].Symbol;
                }
                else
                {
                    CurrencyCode = Currencies[0].Code;
                    CurrencySymbol = Currencies[0].Symbol;
                    CurrencyName = Currencies[0].Name;
                }
            }
        }

        /// <summary>
        /// Vai buscar o primeiro domain da lista e enche a string Domain
        /// </summary>
        public void SetDomain()
        {
            if (TopLevelDomain.Count > 0)
            {
                Domain = TopLevelDomain[0];
            }
        }

       
    }
}
