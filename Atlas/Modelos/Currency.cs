﻿namespace Atlas.Modelos
{
    /// <summary>
    /// Classe currency recebe strings do Code, Name e Symbol
    /// </summary>
    public class Currency
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
    }
}