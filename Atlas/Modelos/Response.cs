﻿namespace Atlas.Modelos
{
    /// <summary>
    /// Classe responsável por receber a informação da API e dar feedback acerca do seu sucesso
    /// </summary>
    public class Response
    {

        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public object Result { get; set; }

    }
}
