﻿namespace Atlas
{
    using Modelos;
    using Servicos;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class FormAtlas : Form
    {
        private List<Pais> Paises;
        private NetworkService networkService;
        private ApiService apiService;
        private DialogService dialogService;
        private DataService dataService;

        /// <summary>
        /// Inicializar os serviços no arranque do formulário
        /// </summary>
        public FormAtlas()
        {
            InitializeComponent();
            Paises = new List<Pais>();
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            LoadPaises();
        }

        /// <summary>
        /// Testa a conexão à internet para decidir onde vai buscar a informação, ou da API(c/internet) ou do SQLite(s/internet)
        /// </summary>
        private async void LoadPaises()
        {
            bool load;

            LblResultado.Text = "Loading...";

            var connection = networkService.CheckConnection();
            if (!connection.IsSuccess)
            {
                LoadLocalRates();
                load = false;
            }
            else
            {
                await LoadApiPaises();
                load = true;
            }

            if (Paises.Count == 0)
            {
                LblResultado.Text = "An internet connection is required.";
                return;
            }
           

            if (load)
            {
                LblResultado.Text = string.Format("Database loaded successfully from the internet on " + DateTime.Now.ToString("G"));
            }
            else
            {
                LblResultado.Text = string.Format("Database loaded locally! " + dataService.Atualizacao + ".");
            }

            CmbBoxPais.Enabled = true; //TORNA VISÍVEL A COMBOBOX QUANDO ACABAR DE CARREGAR OS DADOS
            CmbBoxPais.DataSource = Paises;
            CmbBoxPais.DisplayMember = "Name";
            CmbBoxPais.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            CmbBoxPais.AutoCompleteSource = AutoCompleteSource.ListItems;


        }

        /// <summary>
        /// Vai buscar os dados da API
        /// </summary>
        /// <returns></returns>
        private async Task LoadApiPaises()
        {
            var response = await apiService.GetCountriesAsync("https://restcountries.eu", "rest/v2/all");

            Paises = (List<Pais>)response.Result;

            dataService.DeleteData(); 
            dataService.SaveData(Paises, ProgBarLoading); 
        }

        /// <summary>
        /// Vai buscar os dados à base de dados local
        /// </summary>
        private void LoadLocalRates()
        {
            Paises = dataService.GetData(ProgBarLoading);
        }
        

        /// <summary>
        /// Atualiza a imagem e a informação nas Labels do Form
        /// </summary>
        private void AtualizaDados()
        {
            Pais pais = Paises[CmbBoxPais.SelectedIndex];

            if (pais.Name == "French Southern Territories")
            {
                LblNativeName.Font = new Font("Spectral SC", 11, FontStyle.Bold);
                LblNativeName.Location = new Point(40,135);
            }
            else
            {
                LblNativeName.Font = new Font("Spectral SC", 20, FontStyle.Regular);
                LblNativeName.Location = new Point(36,124);

            }

            LblCapital.Text = "Capital : " + pais.Capital;
            LblRegion.Text = pais.Region;
            LblSubRegion.Text = pais.Subregion;
            LblPopulation.Text = "Population : " + pais.Population.ToString();
            LblDemonym.Text = "Demonym : " + pais.Demonym;
            LblArea.Text = "Area : " + pais.Area;
            LblNativeName.Text = pais.NativeName;
            LblCCode.Text = " - " + pais.CurrencyCode;
            LblCName.Text = " - " + pais.CurrencyName;
            LblCSymbol.Text = " - " + pais.CurrencySymbol;
            LblDomain.Text = "Domain : " + pais.Domain;
            LblCioc.Text = "Cioc : " + pais.Cioc;
            LblNumericCode.Text = "Code : " + pais.NumericCode.ToString();

            

            Image img = Image.FromFile(dataService.Imgpath + pais.Name + ".gif");
            PicBoxFlag.Image = img;
            PicBoxFlag.SizeMode = PictureBoxSizeMode.StretchImage;
            PicBoxFlag.BackColor = Color.Green;
            try
            {
                PicBoxMap.BackgroundImage = Image.FromFile(dataService.Imgpath + pais.Name + "Map.png");

            }
            catch (Exception)
            {
                PicBoxMap.BackgroundImage = null;
            }

        } 

        /// <summary>
        /// Atualiza os dados sempre que o item selecionado na combobox é alterado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbBoxPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            AtualizaDados();
        }

        /// <summary>
        /// Fechar o programa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, EventArgs e)
        {
            Dispose();
            Close();
        }

        /// <summary>
        /// Botão com informação sobre o programa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Atlas v1.0.0\nCreated by José Frazão\n10/12/2017", "About");
        }

        /// <summary>
        /// Minimizar o programa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }


        #region Click-to-Move
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        /// <summary>
        /// Click-to-Move
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormAtlas_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion
    }       
}
