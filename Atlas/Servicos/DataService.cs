﻿namespace Atlas.Servicos
{
    using System;
    using System.Collections.Generic;
    using Modelos;
    using System.Data.SQLite;
    using System.IO;
    using System.Net;
    using System.Reflection;
    using ImageMagick;
    using System.Windows.Forms;

    class DataService
    {
        private SQLiteConnection connection;
        private SQLiteCommand command;
        private DialogService dialogService;
        private WebClient webClient = new WebClient();
        private string dbPath;
        private string imgpath;
        private string atualizacao;

        public string Imgpath { get => imgpath; set => imgpath = value; }
        public string DbPath { get => dbPath; set => dbPath = value; }
        public string Atualizacao { get => atualizacao; set => atualizacao = value; }

        /// <summary>
        /// Cria as diretorias necessárias para o funcionamento da aplicação, 
        /// </summary>
        public DataService()
        {
            DbPath = (Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)) + @"\Data\AtlasDB.sqlite";
            Imgpath = (Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)).ToString() + @"\Images\";

            dialogService = new DialogService();

            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            if (!Directory.Exists("Images"))
            {
                Directory.CreateDirectory("Images");
            }

            connection = new SQLiteConnection("Data Source=" + DbPath);
            connection.Open();

            try
            {

                string sqlcommand = "CREATE TABLE IF NOT EXISTS country(Name VARCHAR(50), Capital VARCHAR(50), Region VARCHAR(50), Subregion VARCHAR(50), Population INT, Demonym VARCHAR(15), Area VARCHAR(20), NativeName VARCHAR(100), NumericCode VARCHAR(5), Cioc VARCHAR(5), CurrencyCode VARCHAR(5), CurrencyName VARCHAR(30), CurrencySymbol VARCHAR(5), Domain VARCHAR(5));";

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro - Países", ex.Message);
            }
            connection.Close();
        }

        /// <summary>
        /// Carregar os dados do fichero local de SQLite
        /// </summary>
        /// <returns></returns>
        public List<Pais> GetData(ProgressBar progbar)
        {
            ProgressBar ProgBarLoading = progbar;
            connection.Open();
            List<Pais> Paises = new List<Pais>();
            try
            {
                string sql = "select Name, Capital, Region, Subregion, Population, Demonym, Area, NativeName, NumericCode, Cioc, CurrencyCode, CurrencyName, CurrencySymbol, Domain from country";

                command = new SQLiteCommand(sql, connection);


                ////LÊ CADA REGISTO
               SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Paises.Add(new Pais
                    {
                        Name = (string)reader["Name"],
                        Capital = (string)reader["Capital"],
                        Region = (string)reader["Region"],
                        Subregion = (string)reader["Subregion"],
                        Population = (int)reader["Population"],
                        Demonym = (string)reader["Demonym"],
                        Area = (string)reader["Area"],
                        NativeName = (string)reader["NativeName"],
                        NumericCode = (string)reader["NumericCode"],
                        Cioc = (string)reader["Cioc"],
                        Domain = (string)reader["Domain"],
                        CurrencyCode = (string)reader["CurrencyCode"],
                        CurrencyName = (string)reader["CurrencyName"],
                        CurrencySymbol = (string)reader["CurrencySymbol"]
                    });
                    ProgBarLoading.Value++;
                }
               
                ProgBarLoading.Dispose();
                
                connection.Close();
                Atualizacao = "Last updated on " + File.GetLastWriteTime(DbPath).ToString();

                return Paises;
            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro", ex.Message);
                return null;
            }


        }

        /// <summary>
        /// Inserir os dados no SQLite e converter imagens da lista de países
        /// </summary>
        /// <param name="PaisesSrc"></param>
        public void SaveData(List<Pais> PaisesSrc, ProgressBar progbar)
        {
            ProgressBar ProgBarLoading = progbar;
            connection = new SQLiteConnection("Data Source=" + DbPath);
            connection.Open();

            List<Pais> Paises = new List<Pais>();
            Paises = PaisesSrc;
            try
            {
                foreach (var pais in Paises) //PERCORRER A LISTA
                {
                    pais.AreaNotNull();
                    pais.CiocNotNull();
                    pais.NumericCodeNotNull();
                    pais.SetCurrency();
                    pais.SetDomain();
                    pais.CurrencySymbolNotNull();

                    //PUXAR IMAGEM SVG E CONVERTER PARA BMP
                    if (!File.Exists(Imgpath + pais.Name + ".bmp"))
                    {
                        string url = pais.Flag;
                        webClient.DownloadFile(url, Imgpath + pais.Name + ".svg");  //DOWNLOAD DO SVG
                        webClient.Dispose();                                        //LIBERTAR RECURSOS
                        MagickImage svg = new MagickImage(Imgpath + pais.Name + ".svg");
                        svg.Write(Imgpath + pais.Name + ".gif");
                        File.Delete(Imgpath + pais.Name + ".svg");
                        svg.Dispose();
                        
                        //APAGAR O SVG
                    }
                    if (!File.Exists(Imgpath + pais.Name + "Map.png") && pais.Domain.Length > 0)
                    {
                        string url = "https://raw.githubusercontent.com/djaiss/mapsicon/master/all/" + pais.Domain.Remove(0, 1) + "/256.png";
                        if (pais.Domain == ".uk")
                        {
                            url = "https://raw.githubusercontent.com/djaiss/mapsicon/master/all/gb/1024.png";
                        }
                        try
                        {
                            webClient.DownloadFile(url, Imgpath + pais.Name + "Map.png");  //DOWNLOAD DO SVG
                            webClient.Dispose();
                        }
                        catch (Exception)
                        {
                        }
                    }

                    ////////File.Delete(Imgpath + pais.Name + "Map.png");                  //APAGAR O SVG

                    string sql = String.Format("INSERT INTO country(Name, Capital, Region, Subregion, Population, Demonym, Area, NativeName, NumericCode, Cioc, CurrencyCode, CurrencyName, CurrencySymbol, Domain) values (@Name, @Capital, @Region, @Subregion, @Population, @Demonym, @Area, @NativeName, @NumericCode, @Cioc, @CurrencyCode, @CurrencyName, @CurrencySymbol, @Domain);"); 

                    command = new SQLiteCommand(sql, connection);

                    command.Parameters.AddWithValue("@Name", pais.Name);
                    command.Parameters.AddWithValue("@Capital", pais.Capital);
                    command.Parameters.AddWithValue("@Region", pais.Region);
                    command.Parameters.AddWithValue("@Subregion", pais.Subregion);
                    command.Parameters.AddWithValue("@Population", pais.Population);
                    command.Parameters.AddWithValue("@Demonym", pais.Demonym);
                    command.Parameters.AddWithValue("@Area", pais.Area);
                    command.Parameters.AddWithValue("@NativeName", pais.NativeName);
                    command.Parameters.AddWithValue("@NumericCode", pais.NumericCode);
                    command.Parameters.AddWithValue("@Cioc", pais.Cioc);
                    command.Parameters.AddWithValue("@CurrencyCode", pais.CurrencyCode);
                    command.Parameters.AddWithValue("@CurrencyName", pais.CurrencyName);
                    command.Parameters.AddWithValue("@CurrencySymbol", pais.CurrencySymbol);
                    command.Parameters.AddWithValue("@Domain", pais.Domain);

                    command.ExecuteScalar();
                    ProgBarLoading.Value++;
                }
               
                ProgBarLoading.Dispose();
            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro - Save Data", ex.Message);
            }//Inserir Países

            connection.Close();

        }

        /// <summary>
        /// Apagar por completo a tabela 'country'
        /// </summary>
        public void DeleteData()
        {
            connection = new SQLiteConnection("Data Source=" + DbPath);
            connection.Open();

            try
            {
                string sql = "DELETE FROM country";

                command = new SQLiteCommand(sql, connection);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro", ex.Message);
            }

            connection.Close();

        }

    }

}
