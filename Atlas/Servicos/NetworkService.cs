﻿namespace Atlas.Servicos
{
    using Modelos;
    using System.Net;

    public class NetworkService
    {
        /// <summary>
        /// Testa a conexão à internet
        /// </summary>
        /// <returns></returns>
        public Response CheckConnection()
        {
            var client = new WebClient();

            try
            {
                using (client.OpenRead("http://client3.google.com/generate_204"))
                {
                    return new Response
                    {
                        IsSuccess = true
                    };
                }
            }
            catch
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = "Verifique a sua ligação à Internet!"
                };
            }
        }
    }

}
