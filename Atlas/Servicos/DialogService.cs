﻿namespace Atlas.Servicos
{
    using System.Windows.Forms;

    public class DialogService
    {
        /// <summary>
        /// Responsável por enviar mensagens do estado do programa
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public void ShowMessage(string title, string message)
        {
            MessageBox.Show(message, title);
        }
    }
}
