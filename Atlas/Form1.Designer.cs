﻿namespace Atlas
{
    partial class FormAtlas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAtlas));
            this.LblResultado = new System.Windows.Forms.Label();
            this.CmbBoxPais = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LblCapital = new System.Windows.Forms.Label();
            this.LblRegion = new System.Windows.Forms.Label();
            this.LblSubRegion = new System.Windows.Forms.Label();
            this.LblPopulation = new System.Windows.Forms.Label();
            this.LblDemonym = new System.Windows.Forms.Label();
            this.LblArea = new System.Windows.Forms.Label();
            this.LblNativeName = new System.Windows.Forms.Label();
            this.LblNumericCode = new System.Windows.Forms.Label();
            this.LblCioc = new System.Windows.Forms.Label();
            this.LblCCode = new System.Windows.Forms.Label();
            this.LblCName = new System.Windows.Forms.Label();
            this.LblCSymbol = new System.Windows.Forms.Label();
            this.LblDomain = new System.Windows.Forms.Label();
            this.PicBoxFlag = new System.Windows.Forms.PictureBox();
            this.BtnClose = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ProgBarLoading = new System.Windows.Forms.ProgressBar();
            this.PicBoxMap = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxMap)).BeginInit();
            this.SuspendLayout();
            // 
            // LblResultado
            // 
            this.LblResultado.AutoSize = true;
            this.LblResultado.BackColor = System.Drawing.Color.Transparent;
            this.LblResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblResultado.Location = new System.Drawing.Point(20, 429);
            this.LblResultado.Name = "LblResultado";
            this.LblResultado.Size = new System.Drawing.Size(37, 13);
            this.LblResultado.TabIndex = 2;
            this.LblResultado.Text = "Status";
            // 
            // CmbBoxPais
            // 
            this.CmbBoxPais.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbBoxPais.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbBoxPais.Enabled = false;
            this.CmbBoxPais.Font = new System.Drawing.Font("Spectral SC", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbBoxPais.FormattingEnabled = true;
            this.CmbBoxPais.Location = new System.Drawing.Point(23, 77);
            this.CmbBoxPais.Name = "CmbBoxPais";
            this.CmbBoxPais.Size = new System.Drawing.Size(541, 34);
            this.CmbBoxPais.TabIndex = 1;
            this.CmbBoxPais.SelectedIndexChanged += new System.EventHandler(this.CmbBoxPais_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Perpetua Titling MT", 40F);
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 64);
            this.label1.TabIndex = 3;
            this.label1.Text = "Atlas";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblCapital
            // 
            this.LblCapital.AutoSize = true;
            this.LblCapital.BackColor = System.Drawing.Color.Transparent;
            this.LblCapital.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCapital.Location = new System.Drawing.Point(40, 278);
            this.LblCapital.Name = "LblCapital";
            this.LblCapital.Size = new System.Drawing.Size(53, 17);
            this.LblCapital.TabIndex = 5;
            this.LblCapital.Text = "Capital";
            // 
            // LblRegion
            // 
            this.LblRegion.AutoSize = true;
            this.LblRegion.BackColor = System.Drawing.Color.Transparent;
            this.LblRegion.Font = new System.Drawing.Font("Spectral SC", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRegion.Location = new System.Drawing.Point(39, 161);
            this.LblRegion.Name = "LblRegion";
            this.LblRegion.Size = new System.Drawing.Size(62, 21);
            this.LblRegion.TabIndex = 6;
            this.LblRegion.Text = "Region";
            // 
            // LblSubRegion
            // 
            this.LblSubRegion.AutoSize = true;
            this.LblSubRegion.BackColor = System.Drawing.Color.Transparent;
            this.LblSubRegion.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSubRegion.Location = new System.Drawing.Point(40, 177);
            this.LblSubRegion.Name = "LblSubRegion";
            this.LblSubRegion.Size = new System.Drawing.Size(73, 17);
            this.LblSubRegion.TabIndex = 7;
            this.LblSubRegion.Text = "Sub-Region";
            // 
            // LblPopulation
            // 
            this.LblPopulation.AutoSize = true;
            this.LblPopulation.BackColor = System.Drawing.Color.Transparent;
            this.LblPopulation.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPopulation.Location = new System.Drawing.Point(327, 396);
            this.LblPopulation.Name = "LblPopulation";
            this.LblPopulation.Size = new System.Drawing.Size(77, 17);
            this.LblPopulation.TabIndex = 8;
            this.LblPopulation.Text = "Population";
            // 
            // LblDemonym
            // 
            this.LblDemonym.AutoSize = true;
            this.LblDemonym.BackColor = System.Drawing.Color.Transparent;
            this.LblDemonym.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDemonym.Location = new System.Drawing.Point(40, 304);
            this.LblDemonym.Name = "LblDemonym";
            this.LblDemonym.Size = new System.Drawing.Size(66, 17);
            this.LblDemonym.TabIndex = 9;
            this.LblDemonym.Text = "Demonym";
            // 
            // LblArea
            // 
            this.LblArea.AutoSize = true;
            this.LblArea.BackColor = System.Drawing.Color.Transparent;
            this.LblArea.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblArea.Location = new System.Drawing.Point(327, 383);
            this.LblArea.Name = "LblArea";
            this.LblArea.Size = new System.Drawing.Size(35, 17);
            this.LblArea.TabIndex = 10;
            this.LblArea.Text = "Area";
            // 
            // LblNativeName
            // 
            this.LblNativeName.AutoSize = true;
            this.LblNativeName.BackColor = System.Drawing.Color.Transparent;
            this.LblNativeName.Font = new System.Drawing.Font("Spectral SC", 20F);
            this.LblNativeName.Location = new System.Drawing.Point(36, 124);
            this.LblNativeName.Name = "LblNativeName";
            this.LblNativeName.Size = new System.Drawing.Size(203, 41);
            this.LblNativeName.TabIndex = 11;
            this.LblNativeName.Text = "Native Name";
            // 
            // LblNumericCode
            // 
            this.LblNumericCode.AutoSize = true;
            this.LblNumericCode.BackColor = System.Drawing.Color.Transparent;
            this.LblNumericCode.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNumericCode.Location = new System.Drawing.Point(40, 375);
            this.LblNumericCode.Name = "LblNumericCode";
            this.LblNumericCode.Size = new System.Drawing.Size(91, 17);
            this.LblNumericCode.TabIndex = 12;
            this.LblNumericCode.Text = "Numeric Code";
            // 
            // LblCioc
            // 
            this.LblCioc.AutoSize = true;
            this.LblCioc.BackColor = System.Drawing.Color.Transparent;
            this.LblCioc.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCioc.Location = new System.Drawing.Point(40, 388);
            this.LblCioc.Name = "LblCioc";
            this.LblCioc.Size = new System.Drawing.Size(35, 17);
            this.LblCioc.TabIndex = 13;
            this.LblCioc.Text = "Cioc";
            // 
            // LblCCode
            // 
            this.LblCCode.AutoSize = true;
            this.LblCCode.BackColor = System.Drawing.Color.Transparent;
            this.LblCCode.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCCode.Location = new System.Drawing.Point(50, 347);
            this.LblCCode.Name = "LblCCode";
            this.LblCCode.Size = new System.Drawing.Size(38, 17);
            this.LblCCode.TabIndex = 14;
            this.LblCCode.Text = "Code";
            // 
            // LblCName
            // 
            this.LblCName.AutoSize = true;
            this.LblCName.BackColor = System.Drawing.Color.Transparent;
            this.LblCName.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCName.Location = new System.Drawing.Point(50, 332);
            this.LblCName.Name = "LblCName";
            this.LblCName.Size = new System.Drawing.Size(40, 17);
            this.LblCName.TabIndex = 15;
            this.LblCName.Text = "Name";
            // 
            // LblCSymbol
            // 
            this.LblCSymbol.AutoSize = true;
            this.LblCSymbol.BackColor = System.Drawing.Color.Transparent;
            this.LblCSymbol.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCSymbol.Location = new System.Drawing.Point(50, 362);
            this.LblCSymbol.Name = "LblCSymbol";
            this.LblCSymbol.Size = new System.Drawing.Size(51, 17);
            this.LblCSymbol.TabIndex = 16;
            this.LblCSymbol.Text = "Symbol";
            // 
            // LblDomain
            // 
            this.LblDomain.AutoSize = true;
            this.LblDomain.BackColor = System.Drawing.Color.Transparent;
            this.LblDomain.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDomain.Location = new System.Drawing.Point(40, 291);
            this.LblDomain.Name = "LblDomain";
            this.LblDomain.Size = new System.Drawing.Size(54, 17);
            this.LblDomain.TabIndex = 17;
            this.LblDomain.Text = "Domain";
            // 
            // PicBoxFlag
            // 
            this.PicBoxFlag.Location = new System.Drawing.Point(43, 195);
            this.PicBoxFlag.Name = "PicBoxFlag";
            this.PicBoxFlag.Size = new System.Drawing.Size(120, 80);
            this.PicBoxFlag.TabIndex = 18;
            this.PicBoxFlag.TabStop = false;
            // 
            // BtnClose
            // 
            this.BtnClose.BackColor = System.Drawing.Color.Transparent;
            this.BtnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnClose.BackgroundImage")));
            this.BtnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BtnClose.FlatAppearance.BorderSize = 0;
            this.BtnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnClose.Location = new System.Drawing.Point(552, 12);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(24, 23);
            this.BtnClose.TabIndex = 20;
            this.BtnClose.UseVisualStyleBackColor = false;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(522, 419);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "About";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(188, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "v1.0";
            // 
            // ProgBarLoading
            // 
            this.ProgBarLoading.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ProgBarLoading.Location = new System.Drawing.Point(0, 441);
            this.ProgBarLoading.Maximum = 250;
            this.ProgBarLoading.Name = "ProgBarLoading";
            this.ProgBarLoading.Size = new System.Drawing.Size(588, 10);
            this.ProgBarLoading.TabIndex = 23;
            // 
            // PicBoxMap
            // 
            this.PicBoxMap.BackColor = System.Drawing.Color.Transparent;
            this.PicBoxMap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicBoxMap.Location = new System.Drawing.Point(320, 161);
            this.PicBoxMap.Name = "PicBoxMap";
            this.PicBoxMap.Size = new System.Drawing.Size(220, 220);
            this.PicBoxMap.TabIndex = 24;
            this.PicBoxMap.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Spectral SC", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 317);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 25;
            this.label3.Text = "Currency";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(522, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(24, 23);
            this.button2.TabIndex = 26;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FormAtlas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(588, 451);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ProgBarLoading);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BtnClose);
            this.Controls.Add(this.LblDomain);
            this.Controls.Add(this.LblCSymbol);
            this.Controls.Add(this.LblCName);
            this.Controls.Add(this.LblCCode);
            this.Controls.Add(this.LblCioc);
            this.Controls.Add(this.LblNumericCode);
            this.Controls.Add(this.LblNativeName);
            this.Controls.Add(this.LblArea);
            this.Controls.Add(this.LblDemonym);
            this.Controls.Add(this.LblPopulation);
            this.Controls.Add(this.LblCapital);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CmbBoxPais);
            this.Controls.Add(this.LblResultado);
            this.Controls.Add(this.PicBoxFlag);
            this.Controls.Add(this.PicBoxMap);
            this.Controls.Add(this.LblSubRegion);
            this.Controls.Add(this.LblRegion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormAtlas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Atlas";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormAtlas_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxMap)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label LblResultado;
        private System.Windows.Forms.ComboBox CmbBoxPais;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblCapital;
        private System.Windows.Forms.Label LblRegion;
        private System.Windows.Forms.Label LblSubRegion;
        private System.Windows.Forms.Label LblPopulation;
        private System.Windows.Forms.Label LblDemonym;
        private System.Windows.Forms.Label LblArea;
        private System.Windows.Forms.Label LblNativeName;
        private System.Windows.Forms.Label LblNumericCode;
        private System.Windows.Forms.Label LblCioc;
        private System.Windows.Forms.Label LblCCode;
        private System.Windows.Forms.Label LblCName;
        private System.Windows.Forms.Label LblCSymbol;
        private System.Windows.Forms.Label LblDomain;
        private System.Windows.Forms.PictureBox PicBoxFlag;
        private System.Windows.Forms.Button BtnClose;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar ProgBarLoading;
        private System.Windows.Forms.PictureBox PicBoxMap;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
    }
}

